terraform {
  required_version = ">= 1.0"
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "3.84.0"
    }
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "~> 2.0"
    }
  }
}

provider "google" {
  project     = var.google.project
  region      = var.google.region
  zone        = var.google.zone
  credentials = file(var.google.credentials)
}

provider "cloudflare" {
  email      = var.cloudflare.email
  api_key    = var.cloudflare.api_key
  account_id = var.cloudflare.account_id
}