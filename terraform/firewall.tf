resource "google_compute_firewall" "allow-http" {
  name    = "allow-http"
  network = "default"

  dynamic "allow" {
    for_each = [80, 8001, 8802]
    content {
      protocol = "tcp"
      ports    = [allow.value]
    }
  }

  target_tags = ["http"]
}