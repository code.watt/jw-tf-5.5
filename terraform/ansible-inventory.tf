resource "local_file" "ansible-inventory" {

  content = templatefile("${path.module}/inventory.tmpl",
    {
      ansible_hosts = { for instance in google_compute_instance.django-blog : instance.name => instance.network_interface[0].access_config[0].nat_ip }
  })
  filename = "${path.module}/../ansible/inventory.ini"
}