# Использование
1. Создать и наполнить файл terraform/terraform.tfvars и наполнить нужными данными.
2. В директории ansible выполнить следующее
```
ansible-galaxy install -r requirements.yml
ansible-playbook create_infra.yml
ansible-playbook -i inventory.ini deploy_django-blog.yml
```